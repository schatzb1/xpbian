1. Create a Debian environment with the cinnamon desktop environment. Virtual machines will be fine.
2. In your environment run sudo apt install live-build live-boot live-config
3. mkdir xpbian && cd xpbian && mkdir auto
4. For automatic configuration, run cp /usr/share/doc/live-build/examples/auto/* auto
This guide will assume you are using an automatic configuration. See the live-build manual for details.
5. lb config
6. Download config from this repo
7. Paste the xpbian config folder over top of the generated config
8. Edit config to your needs and return to your project folder when done
9. sudo lb build
10. The finished files will be generated. If no changes were made to config, it will be example.hybrid.iso.
If any errors occur during build, just run the command again. If errors persist, run sudo lb clean
11. After you save the built files, run sudo lb clean
